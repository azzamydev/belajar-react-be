<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

    @livewireStyles
    @vite('resources/js/app.js', 'resources/css/filament.css')
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style>
        body {
            font-family: 'Poppins';
        }

        [x-cloak] {
            display: none !important;
        }
    </style>
</head>

<body class="bg-white">
    <nav class="w-full px-6 py-3 shadow-md flex justify-between items-center bg-primary-500 relative" x-data="{
        subNav: false
    }">
        <ul class="flex items-center gap-x-5">
            <li>
                <a href="#" class="text-white font-bold text-xl">{{ config('app.name') }}</a>
            </li>
            {{-- <li>
                <a href="#">Home</a>
            </li> --}}
        </ul>
        <div class="flex items-center">
            <i x-on:click="subNav = !subNav" class="bi bi-list-nested text-2xl text-white font-extrabold"></i>
        </div>

        {{-- Menu Mobile --}}
        <div x-collapse x-show="subNav" class="absolute right-0 top-14 left-0 p-5 bg-blue-500/30">
            awdad
        </div>
    </nav>

    @livewireScripts
</body>

</html>
