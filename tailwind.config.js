const colors = require("tailwindcss/colors");

module.exports = {
    content: ["./resources/**/*.blade.php", "./vendor/filament/**/*.blade.php"],
    darkMode: "class",
    theme: {
        extend: {
            colors: {
                danger: colors.rose,
                primary: {
                    50: "#EFE6E6",
                    100: "#E1D0D0",
                    200: "#C29E9E",
                    300: "#A46F6F",
                    400: "#774B4B",
                    500: "#472D2D",
                    600: "#382424",
                    700: "#2C1C1C",
                    800: "#1C1212",
                    900: "#100A0A",
                },
                success: colors.green,
                warning: colors.yellow,
            },
        },
    },
    plugins: [
        require("@tailwindcss/forms"),
        require("@tailwindcss/typography"),
    ],
};
