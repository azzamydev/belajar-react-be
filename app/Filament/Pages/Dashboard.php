<?php

namespace App\Filament\Pages;

use Closure;
use Filament\Pages\Page;
use Filament\Facades\Filament;
use Filament\Widgets\AccountWidget;
use Illuminate\Support\Facades\Route;
use Filament\Pages\Dashboard as PagesDashboard;

class Dashboard extends Page
{
    protected static ?string $navigationIcon = 'heroicon-o-home';

    protected static string $view = 'filament.pages.dashboard';

    protected static ?int $navigationSort = -2;

    protected static function getNavigationLabel(): string
    {
        return 'Dashboard';
    }

    public static function getRoutes(): Closure
    {
        return function () {
            Route::get('/', static::class)->name(static::getSlug());
        };
    }

    protected function getHeaderWidgets(): array
    {
        return [
            AccountWidget::class,
        ];
    }

    protected function getColumns(): int | array
    {
        return 2;
    }

    protected function getTitle(): string
    {
        return 'Dashboard';
    }
}
